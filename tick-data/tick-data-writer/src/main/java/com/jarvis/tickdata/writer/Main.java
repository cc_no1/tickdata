/*
 * 09-21-2016   Chen Chen                  Initial version
 */
package com.jarvis.tickdata.writer;

import org.apache.log4j.BasicConfigurator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class Main {

    public static void main(String[] args) {
        BasicConfigurator.configure();
        String configFile = "conf/marketDataWriter.xml";
        if(args.length>0)
            configFile = args[0];
        ApplicationContext context = new FileSystemXmlApplicationContext(configFile);
        
        final MarketDataWriter marketDataWriter = (MarketDataWriter)context.getBean("marketDataWriter");
        try {
            marketDataWriter.init();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            @Override
            public void run(){
                marketDataWriter.uninit();
            }       
        }));

    }

}
