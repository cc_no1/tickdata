/*
 * 09-18-2016   Chen Chen                  Initial version
 */
package com.jarvis.tickdata.writer.format;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import com.jarvis.tickdata.common.data.Quote;
import com.jarvis.tickdata.common.data.TickField;
import com.jarvis.tickdata.common.util.PriceUtils;

public class TickDataFormatter implements ITickDataFormatter {
    private final static SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    private final static DecimalFormat decimalFormat = new DecimalFormat("#.########");
    private final String fieldDelimiter = "=";
    private final String tokenDelimiter = ",";
    
    private int depthRows = 20;

    @Override
    public String fileHeader() {
        StringBuilder sb = new StringBuilder();
        
        sb.append(TickField.SYMBOL.value());
        sb.append(tokenDelimiter);
        sb.append(TickField.TIME.value());
        sb.append(tokenDelimiter);
        sb.append(TickField.BID.value());
        sb.append(tokenDelimiter);
        sb.append(TickField.BID_VOL.value());
        sb.append(tokenDelimiter);
        sb.append(TickField.ASK.value());
        sb.append(tokenDelimiter);
        sb.append(TickField.ASK_VOL.value());
        sb.append(tokenDelimiter);
        sb.append(TickField.LAST.value());
        sb.append(tokenDelimiter);
        sb.append(TickField.LAST_VOL.value());
        sb.append(tokenDelimiter);
        for(int i=0; i<depthRows; i++) {
            sb.append(TickField.BID.value() + i);
            sb.append(tokenDelimiter);
            sb.append(TickField.BID_VOL.value() + i);
            sb.append(tokenDelimiter);
        }
        
        for(int i=0; i<depthRows; i++) {
            sb.append(TickField.ASK.value() + i);
            sb.append(tokenDelimiter);
            sb.append(TickField.ASK_VOL.value() + i);
            sb.append(tokenDelimiter);
        }      
        return sb.toString();
    }

    @Override
    public String quoteToString(Quote quote) {
        StringBuilder sb = new StringBuilder();

        sb.append(quote.getSymbol());
        sb.append(tokenDelimiter);
        
        
        if(null != quote.getTimeStamp()) {
            sb.append(timeFormat.format(quote.getTimeStamp()));
            sb.append(tokenDelimiter);
        } 
        
        double value;
        value = quote.getBid();
        if(!PriceUtils.isZero(value)) {
            sb.append(decimalFormat.format(value));
            sb.append(tokenDelimiter);
        }
        
        value = quote.getBidVol();
        if(!PriceUtils.isZero(value)) {
            sb.append(decimalFormat.format(value));
            sb.append(tokenDelimiter);
        }
        
        value = quote.getAsk();
        if(!PriceUtils.isZero(value)) {
            sb.append(decimalFormat.format(value));
            sb.append(tokenDelimiter);
        }
        
        value = quote.getAskVol();
        if(!PriceUtils.isZero(value)) {
            sb.append(decimalFormat.format(value));
            sb.append(tokenDelimiter);
        }
        
        for(int i=0; i<quote.getBids().size(); i++) {
            sb.append(quote.getBids().get(i).price);
            sb.append(tokenDelimiter);
            
            sb.append(quote.getBids().get(i).quantity);
            sb.append(tokenDelimiter);
        }
        
        for(int i=0; i<quote.getAsks().size(); i++) {
            sb.append(quote.getAsks().get(i).price);
            sb.append(tokenDelimiter);
            
            sb.append(quote.getAsks().get(i).quantity);
            sb.append(tokenDelimiter);
        }
        return sb.toString();
    }

}
