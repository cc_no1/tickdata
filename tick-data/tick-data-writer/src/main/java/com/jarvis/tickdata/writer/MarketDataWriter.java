/*************************************************************************************************************************
 * 09-18-2016   Chen Chen             Initial version
 * 10-12-2016   Chen Chen             Implemented onTrade and persistTrade; Deprecated ITickDataFormatter  
 *************************************************************************************************************************/
package com.jarvis.tickdata.writer;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.jarvis.tickdata.common.data.IMarketDataAdaptor;
import com.jarvis.tickdata.common.data.IMarketDataListener;
import com.jarvis.tickdata.common.data.Quote;
import com.jarvis.tickdata.common.data.Trade;
import com.jarvis.tickdata.common.util.TimeUtil;

public class MarketDataWriter implements IMarketDataListener, IMarketDataWriter {
    private static final Logger log = LoggerFactory
            .getLogger(MarketDataWriter.class);
    
    //parameters for IMarketDataListener
    private Map<String, Quote> quotes = Collections.synchronizedMap(new HashMap<String, Quote>());
    protected Date lastQuoteSent = new Date();
    protected long quoteThrottle = 0; // 0 = no throttle
    protected long timerInterval = 5000;
    protected Map<String, Quote> quotesToBeWritten = new HashMap<String, Quote>();
    protected Map<String, String> clientMarketDataSubscription = new HashMap<String, String>();
    
    private IMarketDataAdaptor adaptor;
    
    //parameters for IMarketDataWriter
    
    private List<String> symbolList = new ArrayList<String>();
    private String directory;
    private Map<String, AsyncWriter> writers = new HashMap<String, AsyncWriter>();
    private boolean newFile;
    private boolean dated;    
    
    private Timer timer = new Timer();
    
    class RunTask extends TimerTask {

        @Override
        public void run() {
          //flush out all quotes throttled
            for(Entry<String, Quote> entry: quotesToBeWritten.entrySet()){
                try {
                    persistQuote(entry.getValue());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                log.debug("Writing throttle quotes: " + entry.getValue());
            }
            quotesToBeWritten.clear();           
        }
        
    }
    
    public MarketDataWriter(IMarketDataAdaptor adaptor) {
        this.adaptor = adaptor;
    }
    
    @Override
    public void init() throws Exception {
        log.info("initialising");
        String dirName = directory;
        File file = new File(dirName);
        if(!file.isDirectory()) {
            log.info("Creating tick directory: " + dirName);
            if(!file.mkdir()) {
                throw new Exception("Unable to create tick data directory: " + dirName);
            }
        } else {
            log.info("Existing tick directory: " + dirName);
        }
        
        RunTask task = new RunTask();
        if(quoteThrottle != 0)
            timer.scheduleAtFixedRate(task, 0, timerInterval);
        
        adaptor.init();
        for(String symbol: symbolList) {
            adaptor.subscribeMarketData(symbol, MarketDataWriter.this);
            
            String symbolDirName = directory + "/" + symbol;
            File symbolFile = new File(symbolDirName);
            if(!symbolFile.isDirectory()) {
                log.info("Creating instrument directory: " + symbolDirName);
                if(!symbolFile.mkdir()) {
                    throw new Exception("Unable to create instrument tick data directory: " + dirName);
                }
            } else {
                log.info("Existing instrument tick directory: " + dirName);
            }
            
        }
        
    }

    @Override
    public void uninit() {
        log.info("uninitialising");
        if(quoteThrottle != 0)
            timer.cancel();
        adaptor.uninit();
        int i = 0;
        for(Entry<String, AsyncWriter> entry: writers.entrySet()) {
            log.info(i+" : " + "closing file for: " + entry.getKey());
            try {
                entry.getValue().close();
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
            i++;
        }
    }

    @Override
    public void onState(boolean on) {
        if (on) {
            log.info("Market data is ready");
        } else {
            log.warn("MarketData feed is down");
        }    
    }

 
    @Override
    public void onQuote(Quote quote) {
        Quote prev = quotes.put(quote.getSymbol(), quote);
        
        if(quote.isEqualAtLevelI(prev)) {
        	return;
        }
     // queue up quotes
        if (null != prev && quoteThrottle != 0 && TimeUtil.getTimePass(prev.getTimeSent()) < quoteThrottle) {
            quote.setTimeSent(prev.getTimeSent()); // important record the last time sent of this quote
            quotesToBeWritten.put(quote.getSymbol(), quote);
            return;
        }
        
     // send the quote now
        quote.setTimeSent(new Date());
        quotesToBeWritten.remove(quote.getSymbol()); //clear anything in queue because we are sending it now
        try {
            persistQuote(quote);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTrade(Trade trade) {
        if(trade == null || trade.getLastVolume() == 0) {
        	return;
        }
        
        try {
            persistTrade(trade);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    
 /*
 *   implementation of IMarketDataWriter
 */

    private AsyncWriter getWriter(String fileName) throws IOException {
        AsyncWriter writer = writers.get(fileName);
        if(null == writer) {
            File file = new File(fileName);
            if(file.exists() && !newFile) {
            	log.info(writers.size() + " : " + "append to existing file : " + fileName);
                writer = new AsyncWriter(file);
                writer.start();
            } else {
            	log.info(writers.size() + " : " + "creating new file : " + fileName);
                file.createNewFile();
                writer = new AsyncWriter(file);
                writer.start();
                CharSequence quote = "Quote";
                CharSequence trade = "Trade";
                String fileHeader;
                if(fileName.contains(quote)){
                    fileHeader = Quote.getFields();
                } else if(fileName.contains(trade)) {
                	fileHeader = Trade.getFields();
                } else {
                	log.error("getWriter: Illegal format of file name");
                	throw new IOException("getWriter: Illegal format of file name");
                }
                writer.asyncWrite(fileHeader);
            }
            writers.put(fileName, writer);
        }
        return writer;
    }
   
    private String getFileName(String symbol, int fileType) {
    	String sdate = dated?"-"+new SimpleDateFormat("yyyyMMdd").format(new Date()): "";
    	String fileName = directory + "/" + symbol + "/" + symbol + sdate;
    	switch (fileType) {
    	case 1:
    		return fileName + "-Quote.csv";
    	case 2:
    		return fileName + "-Trade.csv";
    	default:
    		log.error("getFileName: Undefined file type");
    		return "";
    	}
    }
    
    @Override
    public void persistQuote(Quote quote) throws IOException {
        AsyncWriter writer = getWriter(getFileName(quote.getSymbol(), 1));
        String line = quote.toString();
        writer.asyncWrite(line);   
    }

    @Override
    public void persistTrade(Trade trade) throws IOException {
    	AsyncWriter writer = getWriter(getFileName(trade.getSymbol(), 2));
        String line = trade.toString();
        writer.asyncWrite(line);       
    }
    
    /*
     * getters and setters 
     */
    public long getQuoteThrottle() {
        return quoteThrottle;
    }

    public void setQuoteThrottle(long quoteThrottle) {
        this.quoteThrottle = quoteThrottle;
    }
    
    public List<String> getSymbolList() {
        return symbolList;
    }

    public void setSymbolList(List<String> symbolList) {
        this.symbolList = symbolList;
    }
    
    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }
    
    public boolean isNewFile() {
        return newFile;
    }

    public void setNewFile(boolean newFile) {
        this.newFile = newFile;
    }

    public boolean isDated() {
        return dated;
    }

    public void setDated(boolean dated) {
        this.dated = dated;
    }
    
}