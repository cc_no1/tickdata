/*
 * 09-18-2016   Chen Chen                  Initial version
 */
package com.jarvis.tickdata.writer;

import java.io.IOException;

import com.jarvis.tickdata.common.data.Quote;
import com.jarvis.tickdata.common.data.Trade;

public interface IMarketDataWriter {
    void persistQuote(Quote quote) throws IOException;
    void persistTrade(Trade trade) throws IOException;
}
