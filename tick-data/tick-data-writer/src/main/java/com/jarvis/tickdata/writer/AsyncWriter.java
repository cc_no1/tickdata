/*******************************************************************************************************************
 * 09-22-2016   Chen Chen             Initial version
 * 10-12-2016   Chen Chen             Added Logger member; Deleted unnecessary imported packages;
 *                                    Added method logStat to track the thread.
 ******************************************************************************************************************/
package com.jarvis.tickdata.writer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AsyncWriter {
	private static final Logger log = LoggerFactory
            .getLogger(AsyncWriter.class);
    
    protected List<String> tickBuffer = Collections.synchronizedList(new LinkedList<String>());

    private BufferedWriter writer;
    private boolean running = false;
    private int slow = 100;
    private long eventCount = 0;
    
    AsyncWriter(File file) throws IOException {
        writer = new BufferedWriter(new FileWriter(file, true));
    }
    
    protected void logStat(List<String> queue, String tick) {
    	eventCount++;
    	if(eventCount % 500 == 0) {
    	    log.info(tick);	
    	}
    	if(queue.size() >= slow ) {
    		log.warn("Slow consumer thread " + thread.getName() + " queue size: " + queue.size());
    	}
    }
    
    protected Thread thread = new Thread() {
        @Override
        public void run() {
            while(running) {
                try {
                    synchronized (thread) {
                        if(tickBuffer.size() == 0)
                            thread.wait();
                    }
                } catch (InterruptedException e) {
                    break;
                }
                
                if(tickBuffer.size()>0) {
                    String tick = tickBuffer.remove(0);
                    logStat(tickBuffer, tick);
                    try {
                        writer.write(tick);
                        writer.newLine();
                        writer.flush();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }       
    };
    
    public void asyncWrite(String tickData) {
        synchronized (thread) {
            tickBuffer.add(tickData); 
            thread.notify();
        }
    }
    
    public void start() {
        running = true;
        tickBuffer.clear();
        thread.start();
    }
    
    public void close() throws IOException{
        writer.close();
    }

    public boolean isAlive() {
        return thread.isAlive();
    }

}
