/*
 * 09-18-2016   Chen Chen                  Initial version
 */
package com.jarvis.tickdata.writer.format;

import com.jarvis.tickdata.common.data.Quote;

public interface ITickDataFormatter {
    String fileHeader();
    String quoteToString(Quote quote);
}
