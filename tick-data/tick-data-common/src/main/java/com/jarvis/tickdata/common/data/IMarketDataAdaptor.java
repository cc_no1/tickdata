/*
 * 09-17-2016   Chen Chen                  Initial version
 */
package com.jarvis.tickdata.common.data;

public interface IMarketDataAdaptor {
	public void init() throws Exception;
	public void uninit();
	public boolean getState();
	public void subscribeMarketData(String instrument, IMarketDataListener listener) throws MarketDataException;
	public void unsubscribeMarketData(String instrument, IMarketDataListener listener);
}
