/********************************************************************************************************************************
 * 09-17-2016   Chen Chen                  Initial version
 * 10-12-2016   Chen Chen                  Changed toString to have csv style output; Added static methods getFields;
 *                                         Added new public method isEqualAtLevelI.
 ********************************************************************************************************************************/

package com.jarvis.tickdata.common.data;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jarvis.tickdata.common.type.QtyPrice;
import com.jarvis.tickdata.common.util.PriceUtils;


public class Quote implements Cloneable {
	private static final Logger log = LoggerFactory
			.getLogger(Quote.class);
	
	private final static SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    private final static DecimalFormat decimalFormat = new DecimalFormat("#.########");
	
	String symbol;
	double bid;
	double ask;
	long bidVol;
	long askVol;
	double high;
	double low;
	double open;
	double close;
	Date timeStamp;
	Date timeSent;
	
	public String getSymbol() {
		return symbol;
	}
	public double getBid() {
		return bid;
	}
	public void setBid(double bid) {
		this.bid = bid;
	}
	public double getAsk() {
		return ask;
	}
	public void setAsk(double ask) {
		this.ask = ask;
	}
	public long getBidVol() {
		return bidVol;
	}
	public void setBidVol(long bidVol) {
		this.bidVol = bidVol;
	}
	public long getAskVol() {
		return askVol;
	}
	public void setAskVol(long askVol) {
		this.askVol = askVol;
	}
	public double getHigh() {
		return high;
	}
	public void setHigh(double high) {
		this.high = high;
	}
	public double getLow() {
		return low;
	}
	public void setLow(double low) {
		this.low = low;
	}
	public double getOpen() {
		return open;
	}
	public void setOpen(double open) {
		this.open = open;
	}
	public double getClose() {
		return close;
	}
	public void setClose(double close) {
		this.close = close;
	}

	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	private final static int depthRows = 20;
	protected List<QtyPrice> bids;
	protected List<QtyPrice> asks;
	
	
	public Quote(String symbol, List<QtyPrice> bids, List<QtyPrice> asks) {
		this.symbol = symbol;
		this.bids = bids;
		this.asks = asks;
		this.timeStamp = new Date();
		this.timeSent = this.timeStamp;
	}
	
	public List<QtyPrice> getBids() {
		return bids;
	}
	public List<QtyPrice> getAsks() {
		return asks;
	}
	
	public Date getTimeSent() {
		return timeSent;
	}
	public void setTimeSent(Date timeSent) {
		this.timeSent = timeSent;
	}

    
	public static String getFields() {
        StringBuilder sb = new StringBuilder();
        
        sb.append(TickField.SYMBOL.value());
        sb.append(",");
        sb.append(TickField.TIME.value());
        sb.append(",");
        sb.append(TickField.BID.value());
        sb.append(",");
        sb.append(TickField.BID_VOL.value());
        sb.append(",");
        sb.append(TickField.ASK.value());
        sb.append(",");
        sb.append(TickField.ASK_VOL.value());
        sb.append(",");
        
        for(int i=0; i<depthRows; i++) {
            sb.append(TickField.BID.value() + i);
            sb.append(",");
            sb.append(TickField.BID_VOL.value() + i);
            sb.append(",");
        }
        
        for(int i=0; i<depthRows; i++) {
            sb.append(TickField.ASK.value() + i);
            sb.append(",");
            sb.append(TickField.ASK_VOL.value() + i);
            sb.append(",");
        }      
        return sb.toString();
	}
	
	public boolean isEqualAtLevelI(Quote quote) {
		if(quote == null) {
			return false;
		}
		if( !symbol.equals(quote.getSymbol()) ) {
			return false;
		}
		
		return PriceUtils.Equal(bid, quote.getBid()) 
				&& PriceUtils.Equal(ask, quote.getAsk()) 
				&& PriceUtils.Equal(bidVol, quote.getBidVol()) 
				&& PriceUtils.Equal(askVol, quote.getAskVol());		
	}
	public String toString()
	{
		StringBuilder sb = new StringBuilder();

        sb.append(symbol);
        sb.append(",");
        
        
        if(null != timeStamp) {
            sb.append(timeFormat.format(timeStamp));
        } 
        sb.append(",");
        
        if(!PriceUtils.isZero(bid)) {
            sb.append(decimalFormat.format(bid));
        }
        sb.append(",");

        if(!PriceUtils.isZero(bidVol)) {
            sb.append(decimalFormat.format(bidVol));
        }
        sb.append(",");
        
        if(!PriceUtils.isZero(ask)) {
            sb.append(decimalFormat.format(ask));
        }
        sb.append(",");
        
        if(!PriceUtils.isZero(askVol)) {
            sb.append(decimalFormat.format(askVol));
        }
        sb.append(",");
        
        for(int i=0; i<bids.size(); i++) {
            sb.append(bids.get(i).price);
            sb.append(",");
            
            sb.append(bids.get(i).quantity);
            sb.append(",");
        }
        
        for(int i=0; i<asks.size(); i++) {
            sb.append(asks.get(i).price);
            sb.append(",");
            
            sb.append(asks.get(i).quantity);
            sb.append(",");
        }
        return sb.toString();
	}

	@Override
	public Object clone() { //deep copy
		Quote result;
		try {
			result = (Quote)super.clone();
		} catch (CloneNotSupportedException e) {
			log.error(e.getMessage(), e);
			e.printStackTrace();
			return null;
		}
		result.bids = new LinkedList<QtyPrice>();
		for(QtyPrice qp: bids) {
			result.bids.add(new QtyPrice(qp.quantity, qp.price));
		}
		result.asks = new LinkedList<QtyPrice>();
		for(QtyPrice qp: asks) {
			result.asks.add(new QtyPrice(qp.quantity, qp.price));
		}
		return result;
	}
}
