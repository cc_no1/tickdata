/*
 * 09-18-2016   Chen Chen                  Initial version
 */
package com.jarvis.tickdata.common.data;

import java.io.File;
import java.io.FileFilter;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class RefDataManager implements IRefDataManager {
    private static final Logger log = LoggerFactory
            .getLogger(RefDataManager.class);
    String refDataDir;
    Map<String, RefData> map = new HashMap<String, RefData>();

    @SuppressWarnings("unchecked")
    @Override
    public void init() throws Exception {
        log.info("initialising with directory" + refDataDir);
        XStream xstream = new XStream(new DomDriver());
        File dir = new File(refDataDir);
        if(!dir.exists())
            throw new Exception("Missing refdata directory: " + refDataDir);
        
        FileFilter filter = new FileFilter() {
            @Override
            public boolean accept(File file) {
                String fileName = file.getName().toLowerCase();
                if(file.isFile() && fileName.endsWith(".xml"))
                    return true;
                return false;
            }
            
        };
        File[] files = dir.listFiles(filter);
        for(File file: files) {
            if (file.exists()) {
                RefData refData = (RefData)xstream.fromXML(file);
                map.put(refData.getSymbol(), refData);
            } else {
                throw new Exception("Missing refdata file");
            }
        }
        
    }
    
    @Override
    public void uninit() {
        log.info("uninitialising");
        map.clear();        
    }

    @Override
    public RefData getRefData(String symbol) {
        return map.get(symbol);
    }

    @Override
    public String getRefDataDir() {
        return refDataDir;
    }

    @Override
    public void setRefDataDir(String refDataDir) {
        this.refDataDir = refDataDir;
    }


}
