/*
 * 09-18-2016   Chen Chen                  Initial version
 */
package com.jarvis.tickdata.common.util;

public class PriceUtils {
    private static final int scale = 7;
    private static final double roundingFactor = Math.pow(10, (double) scale);
    private static final double EPSILON = 1.0 / roundingFactor;

    static public boolean Equal(double x, double y) {
        return Math.abs(x - y) < EPSILON;
    }

    static public boolean GreaterThan(double x, double y) {
        return x - y > EPSILON;
    }

    static public boolean LessThan(double x, double y) {
        return y - x > EPSILON;
    }

    static public boolean EqualGreaterThan(double x, double y) {
        return Equal(x, y) || GreaterThan(x, y);
    }

    static public boolean EqualLessThan(double x, double y) {
        return Equal(x, y) || LessThan(x, y);
    }

    static public int Compare(double x, double y) {
        if (Equal(x, y))
            return 0;

        if (GreaterThan(x, y))
            return 1;
        else
            return -1;
    }
    
    static public boolean validPrice(double price) {
        return GreaterThan(price, 0);
    }

    static public boolean isZero(double x) {
        return PriceUtils.Equal(x, 0);
    }
}
