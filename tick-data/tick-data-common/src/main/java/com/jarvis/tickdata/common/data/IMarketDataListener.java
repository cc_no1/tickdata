/*
 * 09-17-2016   Chen Chen                  Initial version
 */

package com.jarvis.tickdata.common.data;

public interface IMarketDataListener {
    void init() throws Exception;
    void uninit();
	void onState(boolean on);
	void onQuote(Quote quote);
	void onTrade(Trade trade);
}
