/*
 * 09-17-2016   Chen Chen                  Initial version
 */

package com.jarvis.tickdata.common.data;

public class MarketDataException extends Exception {
	private static final long serialVersionUID = 7383158339098601099L;

	public MarketDataException(String message) {
		super(message);
	}
}
