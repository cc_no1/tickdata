/*
 * 09-17-2016   Chen Chen                  Initial version
 */
package com.jarvis.tickdata.common.data;

public interface IRefDataManager {
    public void init() throws Exception;
    public void uninit();
    public RefData getRefData(String symbol);
    public String getRefDataDir();
    public void setRefDataDir(String refDataDir);
}
