/*
 * 09-18-2016   Chen Chen                  Initial version
 */
package com.jarvis.tickdata.common.data;

public class RefData extends DataObject {
    public RefData() {
        // default lot size to 1
        put(RefDataField.LOT_SIZE.value(), new Integer(1));
    }
    
    public double roundToLots(double qty) {
        int lotSize = this.getLotSize();
        if (qty > 0)
            return ((long)(qty/lotSize)) * lotSize;
        else
            return 0;
    }
    
    // getters
    public String getSymbol() {
        return this.get(String.class, RefDataField.SYMBOL.value());
    }
    public int getLotSize() {
        return this.get(Integer.class, RefDataField.LOT_SIZE.value());
    }
}
