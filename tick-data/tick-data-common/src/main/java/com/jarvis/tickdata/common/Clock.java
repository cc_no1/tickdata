/*
 * 09-17-2016   Chen Chen                  Initial version
 */
package com.jarvis.tickdata.common;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Clock {
	public enum Mode { AUTO, MANUAL } 
	private static Clock instance;
	private Mode mode = Mode.AUTO;
	private Date manualClock = new Date(0);

	private Clock() {
	}
	public static Clock getInstance() {
		if (null == instance) {
			instance = new Clock();
		}
		return instance;
	}
	public Mode getMode() {
		return mode;
	}
	public void setMode(Mode mode) {
		this.mode = mode;
	}
	
	public Date now() {
		if (mode == Mode.MANUAL)
			return new Date(manualClock.getTime());
		
		return new Date();
	}
	
	public boolean isManual() {
		return mode.equals(Mode.MANUAL);
	}
}
