/****************************************************************************************************************************************
 * 09-17-2016   Chen Chen            Initial version
 * 10-12-2016   Chen Chen            Changed constructor to take care of RTVolume from IB; Changed toString to have csv style output;
 *                                   Added static method getFileds().     
 ****************************************************************************************************************************************/

package com.jarvis.tickdata.common.data;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jarvis.tickdata.common.util.PriceUtils;

public class Trade implements Cloneable {
	private static final Logger log = LoggerFactory
			.getLogger(Trade.class);
	private final static SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    private final static DecimalFormat decimalFormat = new DecimalFormat("#.########");
	
	String symbol;
	double price;
	long lastVolume;
	long timeStamp;
	double vwap;
	long totalVolume;
	boolean isSingleFill;
	
	public Trade(String symbol, String serialized) {
		//To do: change to broker independent constructor
		this.symbol = symbol;
		String[] rtVolume = serialized.split(";");
		price = rtVolume[0].equals("") ? 0.0 : Double.parseDouble(rtVolume[0]);
		lastVolume = rtVolume[1].equals("") ? 0 : Long.parseLong(rtVolume[1]);
		timeStamp = rtVolume[2].equals("") ? 0 : Long.parseLong(rtVolume[2]);
		totalVolume = rtVolume[3].equals("") ? 0 : Long.parseLong(rtVolume[3]);
		vwap = rtVolume[4].equals("") ? 0.0 : Double.parseDouble(rtVolume[4]);
		isSingleFill = rtVolume[5].equals("") ? false : Boolean.parseBoolean(rtVolume[5]);
	}
	
	public static String getFields() {
        StringBuilder sb = new StringBuilder();
        
        sb.append(TickField.SYMBOL.value());
        sb.append(",");
        sb.append(TickField.TIME.value());
        sb.append(",");
        sb.append(TickField.LAST.value());
        sb.append(",");
        sb.append(TickField.LAST_VOL.value());
        sb.append(",");
        sb.append(TickField.TOTAL_VOL.value());
        sb.append(",");
        sb.append("vwap");
        sb.append(",");
        sb.append("filled_by_single");
        sb.append(",");
   
        return sb.toString();
	}
	
	public String toString()
	{
		StringBuilder sb = new StringBuilder();

        sb.append(symbol);
        sb.append(",");
        
        Date date = new Date(timeStamp);
        
        if(date != null) {
            sb.append(timeFormat.format(date));
        } 
        sb.append(",");
        
        if(!PriceUtils.isZero(price)) {
            sb.append(decimalFormat.format(price));
        }
        sb.append(",");
        
        if(lastVolume != 0) {
            sb.append(lastVolume);
        }
        sb.append(",");
        
        if(totalVolume != 0) {
            sb.append(totalVolume);
        }
        sb.append(",");
        
        if(!PriceUtils.isZero(vwap)) {
        	sb.append(vwap);
        }
        sb.append(",");
        
        sb.append(isSingleFill? "true":"false");
        sb.append(",");

        return sb.toString();
	}
	
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public long getLastVolume() {
		return lastVolume;
	}
	public void setLastVolume(long lastVolume) {
		this.lastVolume = lastVolume;
	}
	
	public double getVwap() {
		return vwap;
	}
	
	public void setVwap(double vwap) {
		this.vwap = vwap;		
	}
	
	

	@Override
	public Trade clone() {
		try {
			return (Trade)super.clone();
		} catch (CloneNotSupportedException e) {
			log.error(e.getMessage(), e);
			e.printStackTrace();
		}
		return null;
	}
}
