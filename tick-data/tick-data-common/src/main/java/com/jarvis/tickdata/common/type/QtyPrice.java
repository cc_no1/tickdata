/*
 * 09-17-2016   Chen Chen                  Initial version
 */

package com.jarvis.tickdata.common.type;

public class QtyPrice implements Cloneable {
	public double quantity;
	public double price;
	
	
	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public QtyPrice(double quantity, double price)
	{
		this.quantity = quantity;
		this.price = price;
	}
	
	public String toString(){
		return "[" + quantity + ", " + price + "]";
	}
	
	public QtyPrice clone()
	{
		try {
			return (QtyPrice)super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}
	}
}
