/*********************************************************************************************
 * 09-17-2016   Chen Chen                  Initial version
 * 10-12-2016   Chen Chen                  Implemented tickString to update RTVolume
 *********************************************************************************************/

package com.jarvis.tickdata.adaptor.ib;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ib.client.CommissionReport;
import com.ib.client.Contract;
import com.ib.client.ContractDetails;
import com.ib.client.EClientSocket;
import com.ib.client.EWrapper;
import com.ib.client.EWrapperMsgGenerator;
import com.ib.client.Execution;
import com.ib.client.Order;
import com.ib.client.OrderState;
import com.ib.client.TagValue;
import com.ib.client.TickType;
import com.ib.client.UnderComp;
import com.jarvis.tickdata.common.data.IMarketDataAdaptor;
import com.jarvis.tickdata.common.data.IMarketDataListener;
import com.jarvis.tickdata.common.data.IRefDataManager;
import com.jarvis.tickdata.common.data.MarketDataException;
import com.jarvis.tickdata.common.data.Quote;
import com.jarvis.tickdata.common.data.RefData;
import com.jarvis.tickdata.common.data.RefDataField;
import com.jarvis.tickdata.common.data.Trade;
import com.jarvis.tickdata.common.type.QtyPrice;
import com.jarvis.tickdata.common.util.DualMap;

public class IbAdaptor implements EWrapper, IMarketDataAdaptor {
	private static final Logger log = LoggerFactory.getLogger(IbAdaptor.class);
	
	IRefDataManager refDataManager;
	
	private String host;
	private int port;
	private int clientId = 101;
	
	protected AtomicInteger nextOrderId = new AtomicInteger();
	
	private boolean reqMarketDepth;
	private String genericTickTags = "100,101,104,105,106,107,165,221,225,233,236,258,293,294,295,318";
	private int depthRows = 20;
	private boolean logMarketData;
	private long eventCount = 0;
	private int frequency = 1000;
	
	protected EClientSocket clientSocket;
	private Map<String, List<IMarketDataListener>> subs = 
			Collections.synchronizedMap(new HashMap<String, List<IMarketDataListener>>());
	
	// caching
	DualMap<String, Integer> symbolToId = new DualMap<String, Integer>();
	Map<String, Quote> quotes = Collections.synchronizedMap(new HashMap<String, Quote>());
	
	
	public IbAdaptor() {
	    clientSocket = new EClientSocket(this);
	}
		
	
	////////////////////////////////////////////////
	// Begin: implementation of IMarketDataAdaptor
	////////////////////////////////////////////////
	
	@Override
	synchronized public void init() throws Exception {
		while(!clientSocket.isConnected()) {
		    log.debug("Attempting to establish connection to IB TWS/Gateway...");
		    clientSocket.eConnect(host, port, clientId);
		}
		try {
		    Thread.sleep(2000);
		} catch (InterruptedException e) {
		    log.warn(e.getMessage(), e);
		}
		
		refDataManager.init();
		
	}

	@Override
	synchronized public void uninit() {
		log.info("Clearing quotes and symbol-to-Id maps");
		quotes.clear();
        symbolToId.clear();
	    log.info("disconnecting from IB");
	    refDataManager.uninit();
		clientSocket.eDisconnect();
	}

	@Override
	public boolean getState() {
	    return clientSocket.isConnected();
	}

	@Override
	public void subscribeMarketData(String instrument, IMarketDataListener listener) throws MarketDataException {
	    log.info("subscribeMarketData: " + instrument);
        log.debug("Setting refDataManager: " + refDataManager);
        RefData refData = refDataManager.getRefData(instrument);
        if(refData == null) {
            throw new MarketDataException("Symbol " + instrument + " is not found in reference data");
        }
        Contract contract = refData.get(Contract.class, RefDataField.CONTRACT.value());
        if(null == contract) {
            throw new MarketDataException("Symbol " + instrument + " contract is not found in reference data");
        }
        
        List<IMarketDataListener> list = subs.get(instrument);
        if (list == null) {
            list = Collections.synchronizedList(new ArrayList<IMarketDataListener>());
            subs.put(instrument, list);
        }
        
        if(!list.contains(listener)) {
            list.add(listener);
            listener.onState(true);
        }
        
        if(symbolToId.containsKey(refData.getSymbol())) {
            return;
        }
        
        Integer reqId = nextOrderId.getAndIncrement();
        symbolToId.put(instrument, reqId);
        
        List<TagValue> mktDataOptions = new ArrayList<TagValue>();
        clientSocket.reqMktData(reqId, contract, genericTickTags, false, mktDataOptions);
        
        log.info("subscribeMarketData: " + instrument + "," + reqId);
        if(reqMarketDepth) {
            Vector<TagValue> mktDepthOptions = new Vector<TagValue>();
            clientSocket.reqMktDepth( reqId, contract, depthRows, mktDepthOptions);
            log.info("subscribeMarketDepth: " + refData.getSymbol() + "," + reqId);
        }

	}

	@Override
	public void unsubscribeMarketData(String instrument, IMarketDataListener listener) {
	    List<IMarketDataListener> list = subs.get(instrument);
        if (list != null) {
            list.remove(listener);
            if(list.size() == 0) { // no more listeners on this stock, cancel subscription
                if(!symbolToId.containsKey(instrument))
                    return;
                Integer reqId = symbolToId.get(instrument);
                clientSocket.cancelMktData(reqId);
                if(reqMarketDepth) {
                    clientSocket.cancelMktDepth(reqId);
                }
            }
        }
	}
	
    ////////////////////////////////////////////////
    // End: implementation of IMarketDataAdaptor
    ////////////////////////////////////////////////
	
	
	

	@Override
	public void error(Exception e) {
	    log.error("IB-ERR: " + e.getMessage(), e);

	}

	@Override
	public void error(String str) {
	    log.error("IB-ERR: " + EWrapperMsgGenerator.error(str));

	}

	@Override
	public void error(int id, int errorCode, String errorMsg) {
	    log.info("IB-ERR: " + EWrapperMsgGenerator.error(id, errorCode, errorMsg));

	}

	@Override
	public void connectionClosed() {
	    log.info("IB connection closed");
        quotes.clear();
        symbolToId.clear();
        
        for(List<IMarketDataListener> list: subs.values()) {
            if(null != list)
                for(IMarketDataListener listener: list)
                    listener.onState(false);
        }

	}
	
	synchronized private void publishTrade(Trade trade) {
		if(trade != null && trade.getLastVolume() != 0) {
            List<IMarketDataListener> list = subs.get(trade.getSymbol());
            if(null != list) {
                for(IMarketDataListener listener: list)
                    listener.onTrade(trade);
            }
		}
    }
	
	synchronized private void publishQuote(Quote quote) {
        quote = (Quote)quote.clone();
        quote.setTimeStamp(new Date());
        List<IMarketDataListener> list = subs.get(quote.getSymbol());
        if(null != list) {
            for(IMarketDataListener listener: list)
                listener.onQuote(quote);
        }
    }

	@Override
	public void tickPrice(int tickerId, int field, double price, int canAutoExecute) {
	    if(logMarketData)
            log.info(EWrapperMsgGenerator.tickPrice( tickerId, field, price, canAutoExecute));
        String symbol = symbolToId.getKeyByValue(tickerId);
        if(null == symbol) {
            log.error("tickPrice: " + "can't find id in symbolToId map: " + tickerId);
            return;
        }
        Quote quote = quotes.get(symbol);
        if(null == quote) {
            quote = new Quote(symbol, new LinkedList<QtyPrice>() , new LinkedList<QtyPrice>());
            quotes.put(symbol, quote);
        }
        switch(field) {
        case 1: 
            quote.setBid(price);
            break;
        case 2: 
            quote.setAsk(price);
            break;
        default:
        	if(eventCount % frequency == 0) {
                log.debug("tickPrice: " + symbol + " unhandled field type: " + TickType.getField(field) + ", price: " + price);
        	}
        	eventCount++;
        	break;
        }
	}

	@Override
	public void tickSize(int tickerId, int field, int size) {
	    if(logMarketData)
            log.info(EWrapperMsgGenerator.tickSize( tickerId, field, size));
	    String symbol = symbolToId.getKeyByValue(tickerId);
	    if(null == symbol) {
            log.error("tickSize: " + "can't find id in symbolToId map: " + tickerId);
            return;
        }
	    Quote quote = quotes.get(symbol);
        if(null == quote) {
            quote = new Quote(symbol, new LinkedList<QtyPrice>() , new LinkedList<QtyPrice>());
            quotes.put(symbol, quote);
        }
        switch(field) {
        case 0: 
            quote.setBidVol(size);
            publishQuote(quote);
            break;
        case 3: 
            quote.setAskVol(size);
            publishQuote(quote);
            break;
        default:
        	if(eventCount % frequency == 0) {
                log.debug("tickSize: " + symbol + " unhandled field type: " + TickType.getField(field) + ", size: " + size);
        	}
        	eventCount++;
        	break;
        }

	}

	@Override
	public void tickOptionComputation(int tickerId, int field, double impliedVol, double delta, double optPrice,
			double pvDividend, double gamma, double vega, double theta, double undPrice) {
	    if(logMarketData)
            log.info(EWrapperMsgGenerator.tickOptionComputation( tickerId, field, impliedVol, delta, optPrice, pvDividend,
                gamma, vega, theta, undPrice));

	}

	@Override
	public void tickGeneric(int tickerId, int tickType, double value) {
	    if(logMarketData)
            log.info(EWrapperMsgGenerator.tickGeneric(tickerId, tickType, value));

	}

	@Override
	public void tickString(int tickerId, int tickType, String value) {
		if(logMarketData)
            log.info(EWrapperMsgGenerator.tickString( tickerId, tickType, value));
	    String symbol = symbolToId.getKeyByValue(tickerId);
	    if(null == symbol) {
            log.error("tickSize: " + "can't find id in symbolToId map: " + tickerId);
            return;
        }
	    
	    switch(tickType) {
	    case 48:
	    	Trade trade = new Trade(symbol, value);
	    	publishTrade(trade);
	    	break;
	    default:
	    	if(eventCount % frequency == 0) {
	    	    log.debug("tickString: " + symbol + " unhandled tick type: " + TickType.getField(tickType) + ", value: " + value);   	
	    	}
	    	eventCount++;
	    	break;
	    }
	}

	@Override
	public void tickEFP(int tickerId, int tickType, double basisPoints, String formattedBasisPoints,
			double impliedFuture, int holdDays, String futureExpiry, double dividendImpact, double dividendsToExpiry) {
		// TODO Auto-generated method stub

	}

	@Override
	public void orderStatus(int orderId, String status, int filled, int remaining, double avgFillPrice, int permId,
			int parentId, double lastFillPrice, int clientId, String whyHeld) {
		// TODO Auto-generated method stub

	}

	@Override
	public void openOrder(int orderId, Contract contract, Order order, OrderState orderState) {
		// TODO Auto-generated method stub

	}

	@Override
	public void openOrderEnd() {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateAccountValue(String key, String value, String currency, String accountName) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updatePortfolio(Contract contract, int position, double marketPrice, double marketValue,
			double averageCost, double unrealizedPNL, double realizedPNL, String accountName) {

	}

	@Override
	public void updateAccountTime(String timeStamp) {

	}

	@Override
	public void accountDownloadEnd(String accountName) {
		// TODO Auto-generated method stub

	}

	@Override
	public void nextValidId(int orderId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void contractDetails(int reqId, ContractDetails contractDetails) {

	}

	@Override
	public void bondContractDetails(int reqId, ContractDetails contractDetails) {

	}

	@Override
	public void contractDetailsEnd(int reqId) {

	}

	@Override
	public void execDetails(int reqId, Contract contract, Execution execution) {

	}

	@Override
	public void execDetailsEnd(int reqId) {

	}

	@Override
	public void updateMktDepth(int tickerId, int position, int operation, int side, double price, int size) {
	    log.debug(EWrapperMsgGenerator.updateMktDepth(tickerId, position, operation, side, price, size));
	    
	    String symbol = symbolToId.getKeyByValue(tickerId);
        if(null == symbol) {
            log.error("updateMktDepth: " + "can't find id in symbolToId map: " + tickerId);
            return;
        }
        Quote quote = quotes.get(symbol);
        if(null == quote) {
            quote = new Quote(symbol, new LinkedList<QtyPrice>() , new LinkedList<QtyPrice>());
            quotes.put(symbol, quote);
        }
        List<QtyPrice> list = side==0?quote.getAsks():quote.getBids();
        switch(operation) {
        case 0:
            if(position<list.size()) {
                list.add(position, new QtyPrice(size, price));
            } else {
                int inc = list.size() - position;
                for(int i=0; i<inc; i++)
                    list.add(new QtyPrice(0, 0));
                list.add(new QtyPrice(size, price));
            }
            publishQuote(quote);
            break;
        case 1:
            if(position<list.size()) {
                list.set(position, new QtyPrice(size, price));
            } else {
                log.warn("updateMktDepth: update row not found " + position);
            }
            publishQuote(quote);
            break;
        case 2:
            if(position<list.size()) {
                list.remove(position);
            } else {
                // just to remove the last item at the list to get around a IB bug
                if(list.size()>0)
                    list.remove(list.size()-1);
                log.debug("updateMktDepth: delete row not found " + position);
            }
            publishQuote(quote);
            break;
        default:
            log.warn("updateMktDepth: unknown operation " + operation);
        }
	}

	@Override
	public void updateMktDepthL2(int tickerId, int position, String marketMaker, int operation, int side, double price,
			int size) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateNewsBulletin(int msgId, int msgType, String message, String origExchange) {
		// TODO Auto-generated method stub

	}

	@Override
	public void managedAccounts(String accountsList) {
		// TODO Auto-generated method stub

	}

	@Override
	public void receiveFA(int faDataType, String xml) {
		// TODO Auto-generated method stub

	}

	@Override
	public void historicalData(int reqId, String date, double open, double high, double low, double close, int volume,
			int count, double WAP, boolean hasGaps) {
		// TODO Auto-generated method stub

	}

	@Override
	public void scannerParameters(String xml) {
		// TODO Auto-generated method stub

	}

	@Override
	public void scannerData(int reqId, int rank, ContractDetails contractDetails, String distance, String benchmark,
			String projection, String legsStr) {
		// TODO Auto-generated method stub

	}

	@Override
	public void scannerDataEnd(int reqId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void realtimeBar(int reqId, long time, double open, double high, double low, double close, long volume,
			double wap, int count) {
		// TODO Auto-generated method stub

	}

	@Override
	public void currentTime(long time) {
		// TODO Auto-generated method stub

	}

	@Override
	public void fundamentalData(int reqId, String data) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deltaNeutralValidation(int reqId, UnderComp underComp) {
		// TODO Auto-generated method stub

	}

	@Override
	public void tickSnapshotEnd(int reqId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void marketDataType(int reqId, int marketDataType) {
		// TODO Auto-generated method stub

	}

	@Override
	public void commissionReport(CommissionReport commissionReport) {
		// TODO Auto-generated method stub

	}

	@Override
	public void position(String account, Contract contract, int pos, double avgCost) {
		// TODO Auto-generated method stub

	}

	@Override
	public void positionEnd() {
		// TODO Auto-generated method stub

	}

	@Override
	public void accountSummary(int reqId, String account, String tag, String value, String currency) {
		// TODO Auto-generated method stub

	}

	@Override
	public void accountSummaryEnd(int reqId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyMessageAPI(String apiData) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyCompleted(boolean isSuccessful, String errorText) {
		// TODO Auto-generated method stub

	}

	@Override
	public void displayGroupList(int reqId, String groups) {
		// TODO Auto-generated method stub

	}

	@Override
	public void displayGroupUpdated(int reqId, String contractInfo) {
		// TODO Auto-generated method stub

	}
	
    //////////////////////
    // getters and setters
    //////////////////////

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public boolean isReqMarketDepth() {
        return reqMarketDepth;
    }

    public void setReqMarketDepth(boolean reqMarketDepth) {
        this.reqMarketDepth = reqMarketDepth;
    }

    public String getGenericTickTags() {
        return genericTickTags;
    }

    public void setGenericTickTags(String genericTickTags) {
        this.genericTickTags = genericTickTags;
    }

    public int getDepthRows() {
        return depthRows;
    }

    public void setDepthRows(int depthRows) {
        this.depthRows = depthRows;
    }

    public boolean isLogMarketData() {
        return logMarketData;
    }

    public void setLogMarketData(boolean logMarketData) {
        this.logMarketData = logMarketData;
    }

    public IRefDataManager getRefDataManager() {
        return refDataManager;
    }

    public void setRefDataManager(IRefDataManager refDataManager) {
        this.refDataManager = refDataManager;
    }

}
